﻿namespace ChatBot.Enums
{
    public enum FoodCategory
    {
        american,
        asian,
        cafes_coffee,
        fast_food,
        french,
        indian,
        international,
        italian,
        meat,
        mexican,
        pizza,
        seafood,
        steakhouse,
        sushi,
        thai,
        vegetarian,        
        wine_bar,
    }

    public enum NightLifeCategory
    {
        bar,
        jazz_cafe,
        lounge,
        night_club,

        american,
        diner,
        fast_food,
        international,
        sushi,
        wine_bar,
    }

    public enum NonFoodCategory
    {
        architecture,
        art,
        galleries,
        historic_site,
        history,
        memorial_monument,
        military,
        night_club,
        park,
        planetarium,
        religious_site,
        science,
        walking_area,
        zoo_aquarium,
    }

    public enum ShoppingCategory
    {
        antiques,
        department_store,
        fashion,
        furniture_decor,
        gift,
        toy,
    }
}
