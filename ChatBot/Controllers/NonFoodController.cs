﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using ChatBot.Enums;
using ChatBot.Extensions;
using ChatBot.Models;
using ChatBot.Services;
using CsvHelper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Swashbuckle.AspNetCore.Annotations;

namespace ChatBot.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class NonFoodController : ControllerBase
    {
        public NonFoodController()
        {
            Provider = new CsvProvider<NonFood>();
        }

        private CsvProvider<NonFood> Provider { get; }

        [HttpGet]
        public ExtendedEntity<NonFood> Get() => new ExtendedEntity<NonFood>(Provider.GetList().ToList());

        [HttpGet, Route("{category}/by-category")]
        [SwaggerOperation("0 - architecture, 1 - art, 2 - galleries, 3 - historic_site, 4 - history, 5 - memorial_monument," +
            "6 - military, 7 - night_club, 8 - park, 9 - planetarium, 10 - religious_site, 11 - science, 12 - walking_area, 13 - zoo_aquarium")]
        public ExtendedEntity<NonFood> GetByCategory([FromRoute]NonFoodCategory category)
        {
            var nonFoodCategory = GetCategory(category);
            var result = Provider.GetList()
                .Where(x => x.Categories1 == nonFoodCategory || x.Categories2 == nonFoodCategory || x.Categories3 == nonFoodCategory || x.Categories4 == nonFoodCategory)
                .OrderBy(x => x.Rating);
            return new ExtendedEntity<NonFood>(result.ToList());
        }

        private static string GetCategory(NonFoodCategory category)
        {
            var nonFoodCategory = category.ToString();

            switch (nonFoodCategory)
            {
                case "architecture":
                    return $"sightseeings:{nonFoodCategory}";
                case "art":
                    return $"museums:{nonFoodCategory}";
                case "galleries":
                    return nonFoodCategory;
                case "historic_site":
                    return $"sightseeings:{nonFoodCategory}";
                case "history":
                    return $"museums:{nonFoodCategory}";
                case "memorial_monument":
                    return $"sightseeings:{nonFoodCategory}";
                case "military":
                    return $"museums:{nonFoodCategory}";
                case "night_club":
                    return $"nightlife:{nonFoodCategory}";
                case "park":
                    return $"nature:{nonFoodCategory}";
                case "planetarium":
                    return $"museums:{nonFoodCategory}";
                case "religious_site":
                    return $"sightseeings:{nonFoodCategory}";
                case "science":
                    return $"museums:{nonFoodCategory}";
                case "walking_area":
                    return $"sightseeings:{nonFoodCategory}";
                case "zoo_aquarium":
                    return $"nature:{nonFoodCategory}";
                default:
                    return string.Empty;
            }
        }
    }
}
