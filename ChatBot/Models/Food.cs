﻿//using ChatBot.Attributes;

using CsvHelper.Configuration.Attributes;

namespace ChatBot.Models
{
    public class Food
    {
        [Name("_source/name")]
        public string Name { get; set; }

        [Name("title")]
        public string Title { get; set; }

        [Name("_source/address")]
        public string Address { get; set; }

        [Name("categories 1")]
        public string Categories1 { get; set; }

        [Name("categories 2")]
        public string Categories2 { get; set; }

        [Name("categories 3")]
        public string Categories3 { get; set; }

        [Name("categories 4")]
        public string Categories4 { get; set; }

        [Name("Must have to see")]
        public bool? MustHave { get; set; }

        [Name("isTopPlace")]
        public bool? IsTopPlace { get; set; }

        public bool? Usable { get; set; }

        [Name("isEvening")]
        public bool? IsEvening { get; set; }

        [Name("isQuickBreakfast ()")]
        public bool? IsQuickBreakfast { get; set; }

        [Name("isFullBreakfast")]
        public bool? IsFullBreakfast { get; set; }

        [Name("description")]
        public string Description { get; set; }

        [Name("source of description")]
        public string SourceOfDescription { get; set; }

        [Name("Average time")]
        public int? AverageTime { get; set; }

        [Name("lon")]
        public double? Lon { get; set; }

        [Name("lat")]
        public double? Lat { get; set; }

        [Name("_price")]
        public double? Price { get; set; }

        [Name("rating")]
        public double? Rating { get; set; }

        [Name("tel")]
        public string Tel { get; set; }

        [Name("_source.email")]
        public string SourceEmail { get; set; }

        [Name("url")]
        public string Url { get; set; }

        [Name("crosswalkUrlFoursquare")]
        public string CrosswalkUrlFoursquare { get; set; }

        [Name("crosswalkUrlYelp")]
        public string CrosswalkUrlYelp { get; set; }

        [Name("crosswalkUrlTwitter")]
        public string CrosswalkUrlTwitter { get; set; }

        [Name("crosswalkUrlFacebook")]
        public string CrosswalkUrlFacebook { get; set; }

        [Name("crosswalkUrlInstagram")]
        public string CrosswalkUrlInstagram { get; set; }

        [Name("Open Hours")]
        public string OpenHours { get; set; }

        [Name("monday")]
        public string Monday { get; set; }

        [Name("tuesday")]
        public string Tuesday { get; set; }

        [Name("wednesday")]
        public string Wednesday { get; set; }

        [Name("thursday")]
        public string Thursday { get; set; }

        [Name("friday")]
        public string Friday { get; set; }

        [Name("saturday")]
        public string Saturday { get; set; }

        [Name("sunday")]
        public string Sunday { get; set; }

        [Name("wifi")]
        public bool? Wifi { get; set; }

        [Name("seatingOutdoor")]
        public bool? SeatingOutdoor { get; set; }

        [Name("accessibleWheelchair")]
        public bool? AccessibleWheelchair { get; set; }

        [Name("roomPrivate")]
        public bool? RoomPrivate { get; set; }

        [Name("reservations")]
        public bool? Reservations { get; set; }

        [Name("parking")]
        public bool? Parking { get; set; }

        [Name("paymentCashonly")]
        public bool? PaymentCashOnly { get; set; }

        [Name("country")]
        public string Country { get; set; }

        [Name("city")]
        public string City { get; set; }

        [Name("postcode")]
        public string Postcode { get; set; }

        [Name("timezone")]
        public string Timezone { get; set; }

        [Name("ot__source.dressCode")]
        public string OtSourceDressCode { get; set; }

        [Name("ot__source.opentable_link")]
        public string OtSourceOpentableLink { get; set; }

        [Name("ot__source.MenuLink")]
        public string OtSourceMenuLink { get; set; }

        [Name("_source/existence")]
        public string SourceExistence{ get; set; }

        [Name("picture1")]
        public string Picture1 { get; set; }

        [Name("picture2")]
        public string Picture2 { get; set; }

        [Name("picture3")]
        public string Picture3 { get; set; }

        [Name("picture4")]
        public string Picture4 { get; set; }

        [Name("picture5")]
        public string Picture5 { get; set; }
    }
}
